import os
import time
import random
import json
from datetime import datetime

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import anticaptcha
import pandas as pd
import numpy as np

from app import *
import app as my_app


#navegation options
PATH = ".\chromedriver.exe"
options = webdriver.ChromeOptions()
options.add_argument('--disable-extensions')

#def sendToDOM(res, value):

try:
    xl_name = input("Agregue el nombre de su archivo sin comillas y con extension incluida:\n")
#    driver = webdriver.Chrome(executable_path=PATH, chrome_options=options)
#    wait = WebDriverWait(driver, 10)
    #excel file with cuits/cuils
    xl = pd.read_excel(xl_name)
    array = xl.iloc[:, 0].to_numpy()
    rows = len(xl)
    print("Cantidad de datos: ", rows)
    for i in array:
        print("sended cuit: ", int(i))
        my_app.create_cuit(i)
        time.sleep(1)
except FileNotFoundError:
    print("Archivo inexistente.")
    time.sleep(2)

"""
    driver.get("http://www.bcra.gob.ar/BCRAyVos/Situacion_Crediticia.asp")
    cuit = str(i)
    #Captcha response each cuit
    value = anticaptcha.solverCaptcha()
    print("Procesando operacion de CUIT/CUIL: ", cuit+"\nCon captcha: ", value)
    #set param (cuil/cuit)
    wait.until(EC.text_to_be_present_in_element((By.NAME, "CUIT"), ""))
    cuit_field = driver.find_element_by_name("CUIT")
    cuit_field.clear()
    time.sleep(2)
    cuit_field.send_keys(cuit)
    #find html target and set key
    driver.execute_script("document.getElementById('g-recaptcha-response').value='%s'"%value)
    time.sleep(2)
    #wait.until(EC.text_to_be_present_in_element((By.ID, "g-recaptcha-response"), ""))
    #submit
    driver.find_element_by_css_selector("body > div.container > div.contenido > div > div.col-xs-12.col-md-8.col-md-pull-4 > div > form > table > tbody > tr:nth-child(2) > td > button").click()
    time.sleep(5)
    #scrapp
    wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/div/div/div/div/table[1]")))
    data = driver.find_element_by_xpath("/html/body/div/div[2]/div/div/div/div/table[1]")
    if data.text != "":
        print(data.text)
        nombre = driver.find_element_by_xpath("/html/body/div/div[2]/div/div/div/div/table[1]/tbody/tr/td[1]")
        entidad = driver.find_element_by_xpath("/html/body/div/div[2]/div/div/div/div/table[1]/tbody/tr/td[2]")
        situacion = driver.find_element_by_xpath("/html/body/div/div[2]/div/div/div/div/table[1]/tbody/tr/td[4]")
        monto = driver.find_element_by_xpath("/html/body/div/div[2]/div/div/div/div/table[1]/tbody/tr/td[5]")
        data = nombre.text+"&"+entidad.text+"&"+situacion.text+"&"+monto.text+"\n"
        f = open("data.txt", 'a')
        f.write(data)
    f.close()
    else:
        print("Cuit '%s' sin deuda"%cuit)
        f = open("data.txt", 'a')
        f.write("Cuit '%s' Deudas inexistentes"%cuit)
"""
