import time
import random
from anticaptchaofficial.recaptchav2proxyless import *

def solverCaptcha():
    solver = recaptchaV2Proxyless()
    solver.set_verbose(1)
    solver.set_key("9cf36b2ac820a71d1c8bf0129766f568")
    solver.set_website_url("http://www.bcra.gob.ar/BCRAyVos/Situacion_Crediticia.asp")
    solver.set_website_key("6LdwS08UAAAAALS3Vi6zEITCELwuodHhOQLt8lVv")

    captcha_response = solver.solve_and_return_solution()
    if captcha_response != 0 :
        print("Anticaptcha finish,")
    else:
        print("task with error "+solver.error_code)
    return captcha_response


#<div class="g-recaptcha" data-sitekey="6LdwS08UAAAAALS3Vi6zEITCELwuodHhOQLt8lVv">
