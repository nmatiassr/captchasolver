from flask import Flask, request, jsonify, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from task_celery import *

app = Flask(__name__)

with app.app_context():
    current_app.name

app.config['CELERY_BROKER_URL']='amqp://localhost//'
app.config['CELERY_BACKEND'] = 'db+mysql+pymysql://root@localhost/cuits'

db = SQLAlchemy(app)
ma = Marshmallow(app)

celery = make_celery(app)

class Cuit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cuit = db.Column(db.Integer, unique=True)
    nombre = db.Column(db.String(100), default='')
    entidad = db.Column(db.String(50), default='')
    situacion = db.Column(db.String(10), default='')
    monto = db.Column(db.Integer, default='')
    date = db.Column(db.DateTime, default='')
    def __init__(self, cuit, nombre, entidad, situacion, monto, date):
        self.cuit = cuit
        self.nombre = nombre
        self.entidad = entidad
        self.situacion = situacion
        self.monto = monto
        self.date = date

db.create_all()

class CuitSchema(ma.Schema):
    class Meta:
        fields = ('cuit', 'nombre', 'entidad', 'situacion', 'monto', 'date')

cuit_schema = CuitSchema()
cuits_schema = CuitSchema(many=True)

#endpoints
@app.route('/cuits', methods=['POST'])
def create_cuit():
    create_multicuit.delay()
    return 'I`m working with async request :)'


@app.route('/cuits/get', methods=['GET'])
def get_cuits():
    all_cuits = Cuit.query.all()
    return cuits_schema.jsonify(all_cuits)

@celery.task(name='create_cuit')
def create_multicuit():
    cuit = request.json['cuit']
    nombre = request.json['nombre']
    entidad = request.json['entidad']
    situacion = request.json['situacion']
    monto = request.json['monto']
    date = request.json['date']
    new_cuit = Cuit(cuit, nombre, entidad, situacion, monto, date)
    db.session.add(new_cuit)
    db.session.commit()
    return cuit_schema.jsonify(new_cuit)


if __name__ == "__main__":
    app.run(debug=True)
############### CONFIGURAR ESTO ###################
# Abre conexion con la base de datos
